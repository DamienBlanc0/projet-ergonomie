//définit les routes de notre application

'use strict';

//Afin de définir les routes, nous allons avoir besoin d'un module d'AngularJS appelé ngRoute.
angular.module('myApp', [
	'ngRoute',
	'myApp.home',
	'myApp.configurateur'
   
]).	//Le module ngRoute a un composant appelé $routeProvider qui est utile pour configurer les routes.
config(['$routeProvider', function($routeProvider) {
	// Routes will be here
	// Set defualt view of our app to home
	$routeProvider.otherwise({redirectTo: '/home'});
}]);