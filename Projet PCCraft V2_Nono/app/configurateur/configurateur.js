
angular.module('myApp.configurateur', ['ngRoute','firebase'])
 
// Declared route 
.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/configurateur/preparation', {
    	//définit une nouvelle route
        templateUrl: 'configurateur/preparation.html',
        controller: 'PreparationConfigurateurCtrl'
    })
    $routeProvider.when('/configurateur/composants/details', {
        //définit une nouvelle route
        templateUrl: 'configurateur/details_produit.html',
        controller: 'DetailsProduitCtrl'
    })
    $routeProvider.when('/configurateur/composants', {
    	//définit une nouvelle route
        templateUrl: 'configurateur/etape_composants.html',
        controller: 'ComposantsConfigurateurCtrl'
    });
}])

.controller('PreparationConfigurateurCtrl', [function($scope) {

	//var firebaseObj = new Firebase("https://pcraft.firebaseio.com/"); 
	//TODO enregistrer le budget et le profil de configuration
    //TODO ajouter une ligne dans la base

}])

.controller('DetailsProduitCtrl', [function($scope) {

  

}])

.controller("ComposantsConfigurateurCtrl", ["$scope", "$firebaseArray", "$firebaseObject",
    function($scope, $firebaseArray, $firebaseObject) {
       
        var ref = new Firebase("https://pcraft.firebaseio.com/");
        var allObj = $firebaseObject(ref);
       
        $scope.mescomposants = $firebaseArray(ref.child('comfigurations').child('config1').child('produits').child('composants')); 
        $scope.cartesmeres = $firebaseArray(ref.child('catalogue').child('cartesmeres'));
        $scope.limit= 1;
        $scope.maconfig = $firebaseObject(ref.child('comfigurations').child('config1'));
        
      
        //check!
        var macartemere = $firebaseObject(ref.child('comfigurations').child('config1').child('produits').child('composants').child('cartemere'));        
        $scope.ajouterComposant = function($composant) {
            console.log($composant); 

            updateMaConfig($composant);
            macartemere.$bindTo($scope, "macartemere").then(function() {                
                //console.log($scope.macartemere); 
                $scope.macartemere.choix = $composant.nom; 
                $scope.macartemere.depense = parseInt($composant.prix);                                         
            }); 

        }


        var maconfig = $scope.maconfig;
        var updateMaConfig = function($composant) {
            maconfig.$bindTo($scope, "maconfig").then(function() {                
                //console.log($scope.macartemere); 
                $scope.maconfig.cout = parseInt($scope.maconfig.cout) + parseInt($composant.prix);         
                $scope.maconfig.choix_courant = "Processeur";                                       
            });

        }


        //fonction exemple
        var valeur = $firebaseObject(ref.child('valeur'));
        $scope.action = function($n) {
            valeur.$bindTo($scope, "valeur").then(function() {                
                console.log($scope.valeur); 
                $scope.valeur.id = $n;
                console.log($scope.valeur);                 
            });
        }
    
    }
]);
