
angular.module('myApp.home', ['ngRoute','firebase'])
 
// Declared route 
.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/home', {
    	//définit une nouvelle route
        templateUrl: 'home/home.html',
        controller: 'HomeCtrl'
    });
}])
 
// Un controller est une logique qui contrôle une vue en particulier
// Le module $firebaseSimpleLogin est utilisé pour s'authentifier dans Firebase en utilisant l'ID d'email et le mot de passe.
.controller('HomeCtrl', [function($scope) {

	var firebaseObj = new Firebase("https://pcraft.firebaseio.com/"); 
	
}]);
