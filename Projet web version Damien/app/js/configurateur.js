
angular.module('myApp.configurateur', ['ngRoute','firebase'])
 
// Declared route 
.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/configurateur/preparation', {
    	//définit une nouvelle route
        templateUrl: 'views/preparation.html',
        controller: 'PreparationConfigurateurCtrl'
    })
    $routeProvider.when('/composants/details', {
        //définit une nouvelle route
        templateUrl: 'views/details_produit.html',
        controller: 'DetailsProduitCtrl'
    })
    $routeProvider.when('/configurateur/composants', {
    	//définit une nouvelle route
        templateUrl: 'views/etape_composants.html',
        controller: 'ComposantsConfigurateurCtrl'
    })
     $routeProvider.when('/configurateur/recap_composants', {
        //définit une nouvelle route
        templateUrl: 'views/recap_composants.html',
        controller: 'ComposantsConfigurateurCtrl'
    });
}])

.filter('prix', function() {
  return function(input, bool) {
    input = input || '';
    var out = "";
    
    return out;
  };
})

.controller('PreparationConfigurateurCtrl', [function($scope) {

	//var firebaseObj = new Firebase("https://pcraft.firebaseio.com/"); 
	//TODO enregistrer le budget et le profil de configuration
    //TODO ajouter une ligne dans la base

}])

.controller('DetailsProduitCtrl', [function($scope) {

  

}])

.controller("ComposantsConfigurateurCtrl", ["$scope", "$firebaseArray", "$firebaseObject", "filterFilter",
    function($scope, $firebaseArray, $firebaseObject, filterFilter) {
       
        var ref = new Firebase("https://pcraft.firebaseio.com/");
        var allObj = $firebaseObject(ref);

        //Cet index permet de mettre à jour le choix courant. !!Respecter la syntaxe - 0: cartemere / 1: processeur
        var composantRefIndex = 0;             
        var nomNextComposant = ["cartemere", "processeur", "fin_composants"];
        //Récupération des choix de l'utilisateur : estimation + nom du choix (vide au départ) + depense (vide au départ)
        var macartemere = $firebaseObject(ref.child('comfigurations').child('config1').child('produits').child('composants').child('cartemere'));
        var monprocesseur = $firebaseObject(ref.child('comfigurations').child('config1').child('produits').child('composants').child('processeur'));                        

        //Récupération de la configuration en cours de création et de ses composants pour affichage dans la vue
        $scope.maconfig = $firebaseObject(ref.child('comfigurations').child('config1'));
        $scope.mescomposants = $firebaseObject(ref.child('comfigurations').child('config1').child('produits').child('composants'));
        
        //Récupération des produits dans le catalogue à afficher dans la liste de produits
        var cartesmeres = $firebaseArray(ref.child('catalogue').child('composants').child('cartemere'));    
        var processeurs = $firebaseArray(ref.child('catalogue').child('composants').child('processeur'));
        var produits = new Array();
        produits.push(cartesmeres);
        produits.push(processeurs);

        $scope.liste_produits = produits[composantRefIndex];
       
        /* CODE POUR LA COMPARAISON DES PRODUITS */
        $scope.compteur=0;
        $scope.showme = false;

        $scope.compar_element = new Array();

        $scope.plus = function($index){

            if ($scope.compar_element[$index]==null){
                $scope.compar_element[$index]=true;
                $scope.compteur=$scope.compteur+1;
            }
            else if ($scope.compar_element[$index]==false){
                $scope.compar_element[$index]=true;
                $scope.compteur=$scope.compteur+1;
            }

            else{
                $scope.compar_element[$index]=false;
                $scope.compteur=$scope.compteur-1;
            }
        }

        $scope.showexit = function(){
            $scope.showme = false;
        }      
        /* FIN CODE POUR LA COMPARAISON DES PRODUITS */


        $scope.filtrerParPrix = function()
        {   
            $scope.compteur=0;
            $scope.compar_element = new Array();

            $scope.liste_produits = produits[composantRefIndex];
            
            if($("#filtreprix").val() != "")
            {
                var liste_produits_filtre = new Array(); 
                for (var i = 0; i < $scope.liste_produits.length; i++) {             
                    if($scope.liste_produits[i].prix <= $("#filtreprix").val())
                    {                    
                        liste_produits_filtre.push($scope.liste_produits[i]);
                    }
                }; 
                $scope.liste_produits = liste_produits_filtre;
            }     
        }

       
        $scope.ajouterCarteMere = function($composant)
        {            
            updateMaConfig($composant);
            $scope.showme=false;
            $scope.compteur=0;
            $scope.t_c=[null,null];
            $scope.compar_element = new Array();
            macartemere.$bindTo($scope, "macartemere").then(function() {                                
                $scope.macartemere.choix = $composant.nom; 
                $scope.macartemere.depense = parseInt($composant.prix);                                         
            });
        }

        $scope.ajouterProcesseur = function($composant)
        {
            updateMaConfig($composant);
            $scope.showme=false;
            $scope.compteur=0;
            $scope.t_c=[null,null];
            $scope.compar_element = new Array();
            monprocesseur.$bindTo($scope, "monprocesseur").then(function() {                                
                $scope.monprocesseur.choix = $composant.nom; 
                $scope.monprocesseur.depense = parseInt($composant.prix);                                         
            }); 
        }

        //update le cout de la config et le choix courant
        var updateMaConfig = function($composant) {
                        
            $scope.maconfig.cout = parseInt($scope.maconfig.cout) + parseInt($composant.prix);
            if(nomNextComposant[composantRefIndex] != "fin_composants") 
            {
                composantRefIndex++;
                $scope.maconfig.choix_courant = nomNextComposant[composantRefIndex]; 
                $scope.liste_produits = produits[composantRefIndex];
            }
                                   
            $scope.maconfig.$save().then(function(ref) {
              
            }, function(error) {
              console.log("Error:", error);
            });
            


       



        $scope.test = function(){
            $scope.t_c = new Array();
            if ($scope.compteur==2){
                for (var i = 0; i < $scope.compar_element.length; i++) {                    
                    if($scope.compar_element[i] == true)
                    {                    
                        $scope.t_c.push(i);
                    }
                };
                $scope.showme = true;
            }
            else{
                $scope.showme = false;
            }
        }

           /* maconfig.$bindTo($scope, "maconfig").then(function() {                
                $scope.maconfig.cout = parseInt($scope.maconfig.cout) + parseInt($composant.prix);     
                composantRefIndex++;    
                $scope.maconfig.choix_courant = nomNextComposant[composantRefIndex];                                      
                
            });*/
        }

    }
]);
