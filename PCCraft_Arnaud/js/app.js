//définit les routes de notre application

'use strict';

//Afin de définir les routes, nous allons avoir besoin d'un module d'AngularJS appelé ngRoute.
var app = angular.module('myApp', [
	'ngRoute',
	'firebase',
	'myApp.home',
	'myApp.configurateur',
    'myApp.preparation',
	'myApp.compte',
	'myApp.profil'
]);

app.run(["$rootScope", "$location", function($rootScope, $location) {
	$rootScope.$on("$routeChangeError", function(event, next, previous, error) {
		// We can catch the error thrown when the $requireAuth promise is rejected
		// and redirect the user back to the home page
		if (error === "AUTH_REQUIRED") {
			$location.path("/home");
		}
	});
}]);


app.factory("Auth", ["$firebaseAuth",
	function($firebaseAuth) {
		var ref = new Firebase("https://pcraft.firebaseio.com");
		return $firebaseAuth(ref);
	}
]);

//Le module ngRoute a un composant appelé $routeProvider qui est utile pour configurer les routes.
app.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/home', {
    	//définit une nouvelle route
        templateUrl: 'views/home.html',
        controller: 'HomeCtrl'
    });
	$routeProvider.when('/configurateur/preparation', {
        templateUrl: 'views/preparation.html',
        controller: 'PreparationCtrl'
    })
    $routeProvider.when('/composants/details', {
        templateUrl: 'views/details_produit.html',
        controller: 'DetailsProduitCtrl'
    })
    $routeProvider.when('/configurateur/composants', {
        templateUrl: 'views/etape_composants.html',
        controller: 'ConfigurateurCtrl'
    })
     $routeProvider.when('/configurateur/recap_composants', {
        templateUrl: 'views/recap_composants.html',
        controller: 'ConfigurateurCtrl'
    });
	$routeProvider.when('/catalogue', {
        templateUrl: 'views/catalogue.html',
        controller: 'CatalogueCtrl'
    })
	$routeProvider.when('/guide', {
        templateUrl: 'views/guide.html',
        controller: 'GuideCtrl'
    })
	$routeProvider.when('/forum', {
        templateUrl: 'views/forum.html',
        controller: 'ForumCtrl'
    })
	$routeProvider.when('/signin', {
        templateUrl: 'views/signin.html',
        controller: 'CompteCtrl'
    })
	$routeProvider.when('/profil', {
        templateUrl: 'views/profil.html',
        controller: 'ProfilCtrl',
		resolve: {
			"currentAuth": ["Auth", function(Auth) {
				return Auth.$requireAuth();
			}]
		}
    })
	$routeProvider.when('/oubli', {
        templateUrl: 'views/oubli.html',
        controller: 'CompteCtrl'
    })
	$routeProvider.otherwise({redirectTo: '/home'});
}]);

app.factory("Auth", ["$firebaseAuth",
	function($firebaseAuth) {
		var ref = new Firebase("https://pcraft.firebaseio.com");
		return $firebaseAuth(ref);
	}
]);