var appprepa = angular.module('myApp.preparation', ['ngRoute','firebase']);
 
 
// Un controller est une logique qui contrôle une vue en particulier
// Le module $firebaseSimpleLogin est utilisé pour s'authentifier dans Firebase en utilisant l'ID d'email et le mot de passe.
appprepa.controller(['$scope', 's-cache', function($scope, cache)
{
    var onload = function()
    {
        cache.set('my-key', 'my-value');
    };
    
    onload();
}]);


app.service('s-cache', function()
{
    var cache = "toto";
    
    this.get = function()
    {
        console.log(cache);
    };
});
