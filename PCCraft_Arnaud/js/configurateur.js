var appconfig = angular.module('myApp.configurateur', ['ngRoute','firebase']);


appconfig.filter('prix', function() {
  return function(input, bool) {
    input = input || '';
    var out = "";
    
    return out;
  };
});

appconfig.controller("ConfigurateurCtrl", ["$scope", "$firebaseArray", "$firebaseObject", "filterFilter", "s-cache",
    function($scope, $firebaseArray, $firebaseObject, filterFilter, cache) {

        var onload = function()
        {
            $scope.text = cache.get();
        };

        onload();
       
        var ref = new Firebase("https://pcraft.firebaseio.com/");
        var allObj = $firebaseObject(ref);

        //Cet index permet de mettre à jour le choix courant. !!Respecter la syntaxe - 0: cartemere / 1: processeur
        var composantRefIndex = 0;             
        var nomNextComposant = ["cartemere", "processeur", "fin_composants"];
        //Récupération des choix de l'utilisateur : estimation + nom du choix (vide au départ) + depense (vide au départ)
        var macartemere = $firebaseObject(ref.child('comfigurations').child('config1').child('produits').child('composants').child('cartemere'));
        var monprocesseur = $firebaseObject(ref.child('comfigurations').child('config1').child('produits').child('composants').child('processeur'));                        

        //Récupération de la configuration en cours de création et de ses composants pour affichage dans la vue
        $scope.maconfig = $firebaseObject(ref.child('comfigurations').child('config1'));
        $scope.mescomposants = $firebaseObject(ref.child('comfigurations').child('config1').child('produits').child('composants'));
        
        //Récupération des produits dans le catalogue à afficher dans la liste de produits
        $scope.composants = $firebaseArray(ref.child('catalogue').child('composants'));
		$scope.peripheriques = $firebaseArray(ref.child('catalogue').child('peripheriques')); 
		$scope.logiciels = $firebaseArray(ref.child('catalogue').child('logiciels')); 
		
		$scope.budgetCheckBox = false;
		$scope.config = [];
		$scope.config.budget = null;
		$scope.config.type = null;
		$scope.config.total = 0;
		$scope.config.composants = [];
		$scope.composants.$loaded().then(function(){
			angular.forEach($scope.composants, function(composant) {
				$scope.config.composants.push({
					nom: composant.nom,
					estimation: composant.estimation,
					prix: "-",
					choix: null
				});
			})
		});
		
		
        /* CODE POUR LA COMPARAISON DES PRODUITS */
		
        $scope.compteur = 0;
        $scope.showme = false;

        $scope.compar_element = new Array();

        $scope.plus = function($index){

            if ($scope.compar_element[$index]==null){
                $scope.compar_element[$index]=true;
                $scope.compteur=$scope.compteur+1;
            }
            else if ($scope.compar_element[$index]==false){
                $scope.compar_element[$index]=true;
                $scope.compteur=$scope.compteur+1;
            }

            else{
                $scope.compar_element[$index]=false;
                $scope.compteur=$scope.compteur-1;
            }
        }

        $scope.test = function(){
            $scope.t_c = new Array();
            if ($scope.compteur==2){
                for (var i = 0; i < $scope.compar_element.length; i++) {                    
                    if($scope.compar_element[i] == true)
                    {                    
                        $scope.t_c.push(i);
                    }
                };
                $scope.showme = true;
            }
            else{
                $scope.showme = false;
            }
        }

        $scope.showexit = function(){
            $scope.showme = false;
        }      

        /* FIN CODE POUR LA COMPARAISON DES PRODUITS */

        $scope.filtrerParPrix = function()
        {   
            $scope.compteur=0;
            $scope.compar_element = new Array();
            //$scope.cartesmeres = $firebaseArray(ref.child('catalogue').child('cartesmeres')); 
            $scope.maconfig.filtre="prix";  
            $scope.maconfig.$save().then(function(ref) {            

            }, function(error) {
              console.log("Error:", error);
            });

            if($scope.maconfig.choix_courant == "cartemere")
            {
                $scope.cartesmeres_filtre = new Array();
                for (var i = 0; i < $scope.cartesmeres.length; i++) {                    
                    if($scope.cartesmeres[i].prix < $("#filtreprix_cm").val())
                    {                    
                        $scope.cartesmeres_filtre.push($scope.cartesmeres[i]);
                    }
                };                 
            
            }
            else if($scope.maconfig.choix_courant == "processeur")
            {
                $scope.processeurs_filtre = new Array();
                for (var i = 0; i < $scope.processeurs.length; i++) {                    
                    if($scope.processeurs[i].prix < $("#filtreprix_proc").val())
                    {                    
                        $scope.processeurs_filtre.push($scope.processeurs[i]);
                    }
                }; 
            }
            

            if($("#filtreprix_cm").val() == "" || $("#filtreprix_proc").val() == "")
            {
                $scope.maconfig.filtre="null";  
                $scope.maconfig.$save().then(function(ref) {

                }, function(error) {
                  console.log("Error:", error);
                });    
            }
                    
        }



        $scope.ajouterCarteMere = function($composant)
        {            
            updateMaConfig($composant);
            $scope.showme=false;
            $scope.compteur=0;
            $scope.t_c=[null,null];
            $scope.compar_element = new Array();
            macartemere.$bindTo($scope, "macartemere").then(function() {                                
                $scope.macartemere.choix = $composant.nom; 
                $scope.macartemere.depense = parseInt($composant.prix);                                         
            });
        }

        $scope.ajouterProcesseur = function($composant)
        {
            updateMaConfig($composant);
            $scope.showme=false;
            $scope.compteur=0;
            $scope.t_c=[null,null];
            $scope.compar_element = new Array();
            monprocesseur.$bindTo($scope, "monprocesseur").then(function() {                                
                $scope.monprocesseur.choix = $composant.nom; 
                $scope.monprocesseur.depense = parseInt($composant.prix);                                         
            }); 
        }

        //update le cout de la config et le choix courant
        var updateMaConfig = function($composant) {
                        
            $scope.maconfig.cout = parseInt($scope.maconfig.cout) + parseInt($composant.prix);
            if(composantRefIndex != "fin_composants") 
            {
                composantRefIndex++;
                $scope.maconfig.choix_courant = nomNextComposant[composantRefIndex]; 
            }
                                   
            $scope.maconfig.$save().then(function(ref) {
              
            }, function(error) {
              console.log("Error:", error);
            });
            

           /* maconfig.$bindTo($scope, "maconfig").then(function() {                
                $scope.maconfig.cout = parseInt($scope.maconfig.cout) + parseInt($composant.prix);     
                composantRefIndex++;    
                $scope.maconfig.choix_courant = nomNextComposant[composantRefIndex];                                      
                
            });*/
        }

    }
]);
