var apppreparation = angular.module('myApp.preparation', ['ngRoute','firebase']);

apppreparation.directive('numberOnlyInput', function () {
	return {
        restrict: 'EA',
        template: '<input ng-model="inputValue"  maxlength="5" size="5"/>',
        scope: {
            inputValue: '='
        },
		link: function (scope) {
            scope.$watch('inputValue', function(newValue,oldValue) {
                var arr = String(newValue).split("");
                if (arr.length === 0) return;
                if (arr.length === 1 && (arr[0] == '-' || arr[0] === '.' )) return;
                if (arr.length === 2 && newValue === '-.') return;
                if (isNaN(newValue)) {
                    scope.inputValue = oldValue;
                }
            });
		}
	};
});

apppreparation.controller("PreparationCtrl", ["$rootScope", "$scope", "$location", "$firebaseArray", "$firebaseObject",
	function($rootScope, $scope, $location, $firebaseArray, $firebaseObject) {
		$scope.budgetCheckBox = false;
		$rootScope.config.data = {};
		$rootScope.config.data.type = null;
		$rootScope.config.data.budget = null;		
	}
]);