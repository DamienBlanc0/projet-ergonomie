//définit les routes de notre application

'use strict';

//Afin de définir les routes, nous allons avoir besoin d'un module d'AngularJS appelé ngRoute.
var app = angular.module('myApp', [
	'ngRoute',
	'firebase',
	'myApp.home',
	'myApp.preparation',
	'myApp.configurateur',
	'myApp.compte',
	'myApp.profil',
	'myApp.panier',
	'myApp.commande',
	'myApp.historique'
]);

app.run(["$rootScope", "$location", function($rootScope, $location) {
	$rootScope.config = {};
	$rootScope.panier = {};
	$rootScope.panier.total = 0;
	$rootScope.panier.commande = [];
	$rootScope.authData = null;
	$rootScope.connected = false;
	$rootScope.$on("$routeChangeError", function(event, next, previous, error) {
		// We can catch the error thrown when the $requireAuth promise is rejected
		// and redirect the user back to the home page
		if (error === "AUTH_REQUIRED") {
			$location.path("/home");
		}
	});
}]);


app.factory("Auth", ["$firebaseAuth",
	function($firebaseAuth) {
		var ref = new Firebase("https://pcraft.firebaseio.com");
		return $firebaseAuth(ref);
	}
]);

app.factory('TypeBudget', function () {
     var data = {
        type: null,
        budget: null
    };
    return data;
});

//Le module ngRoute a un composant appelé $routeProvider qui est utile pour configurer les routes.
app.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/home', {
    	//définit une nouvelle route
        templateUrl: 'views/home.html',
        controller: 'HomeCtrl'
    });
	$routeProvider.when('/configurateur/preparation', {
        templateUrl: 'views/preparation.html',
        controller: 'PreparationCtrl'
    })
    $routeProvider.when('/composants/details', {
        templateUrl: 'views/details_produit.html',
        controller: 'DetailsProduitCtrl'
    })
    $routeProvider.when('/configurateur/composants', {
        templateUrl: 'views/etape_composants.html',
        controller: 'ConfigurateurCtrl'
    })
     $routeProvider.when('/configurateur/recap_composants', {
        templateUrl: 'views/recap_composants.html',
        controller: 'ConfigurateurCtrl'
    })
	$routeProvider.when('/catalogue', {
        templateUrl: 'views/catalogue.html',
        controller: 'CatalogueCtrl'
    })
	$routeProvider.when('/guide', {
        templateUrl: 'views/guide.html',
        controller: 'GuideCtrl'
    })
	$routeProvider.when('/forum', {
        templateUrl: 'views/forum.html',
        controller: 'ForumCtrl'
    })
    $routeProvider.when('/panier', {
        templateUrl: 'views/panier.html',
        controller: 'PanierCtrl'
    })
	$routeProvider.when('/signin', {
        templateUrl: 'views/signin.html',
        controller: 'CompteCtrl'
    })
	$routeProvider.when('/profil', {
        templateUrl: 'views/profil.html',
        controller: 'ProfilCtrl',
		resolve: {
			"currentAuth": ["Auth", function(Auth) {
				return Auth.$requireAuth();
			}]
		}
    })
	$routeProvider.when('/oubli', {
        templateUrl: 'views/oubli.html',
        controller: 'CompteCtrl'
    })
	$routeProvider.when('/panier', {
        templateUrl: 'views/panier.html',
        controller: 'PanierCtrl'
    })
	$routeProvider.when('/commande', {
        templateUrl: 'views/commande.html',
        controller: 'CommandeCtrl',
		resolve: {
			"currentAuth": ["Auth", function(Auth) {
				return Auth.$requireAuth();
			}]
		}
    })
	$routeProvider.when('/historique', {
        templateUrl: 'views/historique.html',
        controller: 'HistoriqueCtrl',
		resolve: {
			"currentAuth": ["Auth", function(Auth) {
				return Auth.$requireAuth();
			}]
		}
    })
	$routeProvider.otherwise({redirectTo: '/home'});
}]);

app.factory("Auth", ["$firebaseAuth",
	function($firebaseAuth) {
		var ref = new Firebase("https://pcraft.firebaseio.com");
		return $firebaseAuth(ref);
	}
]);