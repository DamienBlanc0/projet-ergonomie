﻿var apppanier = angular.module('myApp.panier', ['ngRoute','firebase']);


appconfig.controller("PanierCtrl", ["$rootScope", "$scope", "$location", "$firebaseArray", "$firebaseObject",
    function($rootScope, $scope, $location, $firebaseArray, $firebaseObject) {
       
        var ref = new Firebase("https://pcraft.firebaseio.com/");
		$scope.promotion = false;
		$scope.codepromotion = null;
		$scope.error = null;
		
		$scope.ajoutPromotion = function() {
			if ($scope.codepromotion == "G3J06") {
				if ($rootScope.panier.total != 0) {
					$rootScope.panier.total = $rootScope.panier.total - 5;
					$scope.promotion = true;
				} else {
					$scope.error = "Vous n'avez pas de produit dans votre panier";
				}
			} else {
				$scope.error = "Code non valide"
			}
			$scope.codepromotion = null;
		}
		
		$scope.deleteProduit = function(produit) {
			$rootScope.panier.total = $rootScope.panier.total - produit.prix*produit.quantite;
			$rootScope.panier.commande.splice($rootScope.panier.commande.indexOf(produit), 1);
		}
		
		$rootScope.$watch('panier.total', function() {
			$rootScope.panier.total = Math.round($rootScope.panier.total * 100) / 100;
			if ($rootScope.panier.total < 0) {
				$rootScope.panier.total = 0;
			}
		});
		
		$scope.suppressionQuantite = function(produit) {
			var index = $rootScope.panier.commande.indexOf(produit);
			if ($rootScope.panier.commande[index].quantite > 0) {
				$rootScope.panier.commande[index].quantite = $rootScope.panier.commande[index].quantite - 1;
				$rootScope.panier.total = $rootScope.panier.total - $rootScope.panier.commande[index].prix;
			}
		}
		
		$scope.ajoutQuantite = function(produit) {
			var index = $rootScope.panier.commande.indexOf(produit);
			$rootScope.panier.commande[index].quantite = $rootScope.panier.commande[index].quantite + 1;
			$rootScope.panier.total = $rootScope.panier.total + $rootScope.panier.commande[index].prix;
		}
		
		$scope.passerCommande = function() {
			var empty = true;
			$scope.error = null;
			for(var prop in $scope.panier.commande) {
				if ($scope.panier.commande.hasOwnProperty(prop)) {
					empty = false;
				}
			}
			if (empty) {
				$scope.error = "Votre panier est vide !";
			} else {
				var quantite = false;
				for (var produit in $scope.panier.commande) {
					if ($scope.panier.commande.hasOwnProperty(produit)) {
						if ($scope.panier.commande[produit].quantite > 0) {
							quantite = true;
						}
					}
				}
				if (!quantite) {
					$scope.error = "Vous devez commander au moins un produit (toutes les quantités sont à 0)";
				}
			}
			if ($scope.error == null) {
				console.log($rootScope.authData);
				if ($rootScope.authData == null) {
					$scope.error = "Vous devez vous connecter pour passer la commande";
				} else {
					$location.path("/commande");
				}
			}
		}
		
    }
]);
