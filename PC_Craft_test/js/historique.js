﻿var apphistorique = angular.module('myApp.historique', ['ngRoute','firebase']);
 

// and use it in our controller
apphistorique.controller("HistoriqueCtrl", ["$rootScope", "$scope", "$location", "$firebaseObject",
	function($rootScope, $scope, $location, $firebaseObject) {
	
		var ref = new Firebase("https://pcraft.firebaseio.com/");
		var commandesRaw = $firebaseObject(ref.child('users').child($rootScope.authData.uid).child('commandes'));
		$scope.commandes = [];
		$scope.details = null;
		$scope.showdetails = false;
		
		// récupère les commandes de l'utilisateur
		commandesRaw.$loaded().then(function() {
			angular.forEach(commandesRaw, function(value, key) {
				var timestamp = key;
				var commande = value;
				var date = new Date(timestamp*1000);
				date = date.toGMTString();
				var somme = 0;
				var produits = [];
				angular.forEach(commande, function(value, key) {
					var idproduit = key;
					var produit = value;
					produits.push({
						nom: produit.choix,
						quantite: produit.quantite,
						prix: produit.quantite*produit.prix
					});
					somme = somme + produit.quantite*produit.prix;
				});
				
				$scope.commandes.push({
					reference: timestamp,
					date: date,
					prix: somme,
					etat: "commande expédiée",
					produits: produits
				});
			});
			console.log($scope.commandes);
		});
		
		$scope.showDetails = function(commande) {
			$scope.showdetails = true;
			$scope.details = commande;
		}
		
		$scope.hideDetails = function() {
			$scope.showdetails = false;
			$scope.details = null;
		}		
	}
]);