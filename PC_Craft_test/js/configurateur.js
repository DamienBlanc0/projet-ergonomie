var appconfig = angular.module('myApp.configurateur', ['ngRoute','firebase']);

// filter pour trier sur des objets au lieu d'arrays
app.filter('orderObjectBy', function() {
	return function(items, field, reverse) {
		var filtered = [];
		angular.forEach(items, function(item) {
			filtered.push(item);
		});
		filtered.sort(function (a, b) {
			return (a[field] > b[field] ? 1 : -1);
		});
		if(reverse) filtered.reverse();
			return filtered;
	};
});


appconfig.controller("ConfigurateurCtrl", ["$rootScope", "$scope", "$firebaseArray", "$firebaseObject", "filterFilter",
    function($rootScope, $scope, $firebaseArray, $firebaseObject, filterFilter) {
        var ref = new Firebase("https://pcraft.firebaseio.com/");
        
        //Récupération des produits dans le catalogue
        $scope.catalogue = $firebaseArray(ref.child('catalogue'));
		
		$scope.config = $rootScope.config;
		$scope.config.total = 0;
		$scope.config.catalogue = [];
		$scope.show = [];
		
		// comparaison
		$scope.modecomparaison = false;
		$scope.comparaisonpropriete = [];
		$scope.comparaison = {};
		$scope.nbcheckbox = 0;
		
		var nbcategories = 0;
		
		$scope.catalogue.$loaded().then(function() {
			angular.forEach($scope.catalogue, function(categorie) {
				if (categorie.$id == "composants") {
					$scope.show[categorie.$id] = true;
				} else {
					$scope.show[categorie.$id] = false;
				}
				$scope.config.catalogue[$scope.catalogue.$keyAt(categorie)] = [];
				angular.forEach(categorie, function(produit) {
					if (typeof(produit) == "object" && produit != null) {
						$scope.config.catalogue[$scope.catalogue.$keyAt(categorie)][produit.infos.id] = {
							nom: produit.infos.nom,
							estimation: produit.infos.estimation,
							choix: null,
							prix: null,
							dispo: null,
							quantite: null
						};
					}
				});
			});
			$scope.prochaineCategorie();
		});
		
		var produitcourantid = 0;
		$scope.produitcourantkey = null;
		$scope.produitcourant = null;
		$scope.afficherbouton = true;
		$scope.prochainProduit = function() {
			$scope.afficherbouton = true;
			var sortedProduit = Object.keys($scope.categoriecourante).sort();
			if (produitcourantid < sortedProduit.length) {
				while (sortedProduit[produitcourantid].indexOf("$") > -1) {
					produitcourantid++;
				}
				if (sortedProduit[produitcourantid] == $scope.produitcourantkey) {
					produitcourantid++;
				}
				$scope.produitcourant = $scope.categoriecourante[sortedProduit[produitcourantid]];
				$scope.produitcourantkey = sortedProduit[produitcourantid];
				produitcourantid++;
			} else {
				if (categoriecouranteid == 3) {
					$scope.afficherbouton = false;
				} else {
					produitcourantid = 0;
					$scope.produitcourantkey = null;
					$scope.produitcourant = null;
					$scope.prochaineCategorie();
				}
			}
			$scope.setPropriete();
			$scope.modecomparaison = false;
			$scope.comparaison = {};
			$scope.nbcheckbox = 0;
		}
		
		$scope.changerProduit = function(categoriekey, produitkey) {
			if ($scope.categoriecourantekey != categoriekey) {
				$scope.changerCategorie(categoriekey);
			}
			$scope.produitcourantkey = produitkey;
			$scope.produitcourant = $scope.catalogue.$getRecord(categoriekey)[produitkey];
			var sortedProduit = Object.keys($scope.categoriecourante).sort();
			for (var i in sortedProduit) {
				if (sortedProduit[i] == produitkey) {
					produitcourantid = i;
				}
			}
			$scope.setPropriete();
			$scope.modecomparaison = false;
			$scope.comparaison = {};
			$scope.nbcheckbox = 0;
		}
		
		
		var categoriecouranteid = 0;
		$scope.categoriecourantekey = null;
		$scope.categoriecourante = null;
		$scope.prochaineCategorie = function() {
			$scope.categoriecourantekey = $scope.catalogue.$keyAt(categoriecouranteid);
			$scope.categoriecourante = $scope.catalogue.$getRecord($scope.categoriecourantekey);
			categoriecouranteid++;
			$scope.prochainProduit();
			$scope.show[$scope.categoriecourantekey] = true;
		}
		
		$scope.changerCategorie = function(key) {
			$scope.categoriecourantekey = key;
			$scope.categoriecourante = $scope.catalogue.$getRecord(key);
			categoriecouranteid = $scope.catalogue.$indexFor(key);
		}
		
		
		$scope.choixProduit = function(id, nom, prix, dispo) {
			//console.log($scope.config.catalogue[$scope.categoriecourantekey][$scope.produitcourantkey]);
			if ($scope.config.catalogue[$scope.categoriecourantekey][$scope.produitcourantkey].prix != null) {
				$scope.config.total = $scope.config.total - $scope.config.catalogue[$scope.categoriecourantekey][$scope.produitcourantkey].prix;
			}
			$scope.config.catalogue[$scope.categoriecourantekey][$scope.produitcourantkey].choix = nom;			
			$scope.config.catalogue[$scope.categoriecourantekey][$scope.produitcourantkey].prix = prix;		
			$scope.config.catalogue[$scope.categoriecourantekey][$scope.produitcourantkey].dispo = dispo;
			$scope.config.catalogue[$scope.categoriecourantekey][$scope.produitcourantkey].quantite = 1;
			//console.log(id, nom, prix, dispo);
			//console.log($scope.config.catalogue[$scope.categoriecourantekey][$scope.produitcourantkey]);
			$scope.config.total = $scope.config.total + prix;

			//gestion de la barre de progression du budget
			if (typeof($scope.config.data) != "undefined") {
				if ($scope.config.data.budget != null) {
					$("#budget_bar").css('width', (($scope.config.total/$scope.config.data.budget)*100)+'%').attr('aria-valuenow', prix); 
					gestionBudget();
				}
			}
		}
		
		var gestionBudget = function() {

			if($scope.config.total >= $scope.config.data.budget)
			{
				$("#budget_bar").removeClass("progress-bar-success");
				$("#budget_bar").addClass("progress-bar-danger");				
			}
			else if($scope.config.total < $scope.config.data.budget)
			{
				$("#budget_bar").removeClass("progress-bar-danger");
				$("#budget_bar").addClass("progress-bar-success");
			}

		}
		
		$scope.deleteProduit = function(categoriekey, produitkey) {
			$scope.config.total = $scope.config.total - $scope.config.catalogue[categoriekey][produitkey].prix;
			$scope.config.catalogue[categoriekey][produitkey].choix = null;
			$scope.config.catalogue[categoriekey][produitkey].prix = null;
			$scope.config.catalogue[categoriekey][produitkey].dispo = null;

			$("#budget_bar").css('width', (($scope.config.total/600)*100)+'%').attr('aria-valuenow', $scope.config.catalogue[categoriekey][produitkey].prix);
			gestionBudget();
		}
		
		// fonction pour arrondi du coût total à chaque changement
		$scope.$watch('config.total', function() {
			$scope.config.total = Math.round($scope.config.total * 100) / 100;
		});
		
		// fonction déclenchée au moment où l'utilisateur quitte la page
		// met les valeurs de la config  dans le panier
		$scope.leavePage = function() {
			$rootScope.config = $scope.config;
			$rootScope.panier.total = $rootScope.panier.total + $scope.config.total;
			angular.forEach($scope.config, function(catalogue) {
				if (typeof(catalogue) == "object") {
					for (var categorie in catalogue) {
						if (catalogue.hasOwnProperty(categorie)) {
							for (var produit in catalogue[categorie]) {
								if (catalogue[categorie].hasOwnProperty(produit)) {
									if (catalogue[categorie][produit]["choix"] != null) {
										var dejacommande = false;
										for (var produitcommande in $rootScope.panier.commande) {
											if ($rootScope.panier.commande[produitcommande].choix == catalogue[categorie][produit]["choix"]) {
												$rootScope.panier.commande[produitcommande].quantite = $rootScope.panier.commande[produitcommande].quantite + 1;
												dejacommande = true;
											}
										}
										if (!dejacommande) {
											$rootScope.panier.commande.push({
												choix : catalogue[categorie][produit]["choix"],
												dispo : catalogue[categorie][produit]["dispo"],
												nom : catalogue[categorie][produit]["nom"],
												prix : catalogue[categorie][produit]["prix"],
												quantite : catalogue[categorie][produit]["quantite"]
											});
										}
									}
								}
							}
						}
					}
				}
			});
			console.log($rootScope.panier);
			
		}
		
		// filtre par prix
		$scope.prixMaximumFilter = function (produit) {
			var prix = parseFloat(produit.prix);
			var max = parseFloat($scope.prixmaximum);
			if (!prix) {
				return false;
			}
			if(max && prix > max) {
				return false;
			}
			return true;
		};
		
		// filtre par nom
		$scope.nomFiltre = function(produit) {
			if(!$scope.filtrenom) {
				return true;
			} else {
				var keywords = $scope.filtrenom.toLowerCase().split(' ');
				for(var i in keywords) {
					var k = keywords[i];
					if (produit.nom.toLowerCase().indexOf(k) >= 0) {
						return true;
					}
				}
			return false;
			}
		}
		
		// comparaison
		$scope.checkProduit = function(produit) {
			if (produit.nom in $scope.comparaison) {
				delete $scope.comparaison[produit.nom];
			} else {
				$scope.comparaison[produit.nom] = [];
				for (var info in produit) {
					if (info == "caracteristiques") {
						for (var propriete in produit[info]) {
							if (propriete.indexOf("$") == -1) {
								$scope.comparaison[produit.nom].push(produit[info][propriete]);
							}
						}
					} else {
						if (info.indexOf("$") == -1) {
							$scope.comparaison[produit.nom].push(produit[info]);
						}
					}
				}
			}
			$scope.modecomparaison = false;
		}
		
		// fonction pour surveiller le nombre de checkbox comparaison cochées
		$scope.$watch('comparaison', function() {
			var count = 0;
			for (var produit in $scope.comparaison) {
				if ($scope.comparaison.hasOwnProperty(produit)) {
				   count++;
				}
			}
			$scope.nbcheckbox = count;
		}, true);
		
		$scope.setPropriete = function() {
			$scope.comparaisonpropriete = [];
			for (var produit in $scope.produitcourant) {
				if (produit != "infos" && $scope.comparaisonpropriete.length == 0) {
					for (var info in $scope.produitcourant[produit]) {
						if (info == "caracteristiques") {
							for (var propriete in $scope.produitcourant[produit][info]) {
								if (propriete.indexOf("$") == -1) {
									$scope.comparaisonpropriete.push(propriete);
								}
							}
						} else {
							if (info.indexOf("$") == -1) {
								$scope.comparaisonpropriete.push(info);
							}
						}
					}
				}
			}
		console.log($scope.comparaisonpropriete);
		}
		
    }
]);
